import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Navbar from './Navbar';
import Content from './Content';
import Game from './game/Game';
import Users from './user_list/Users';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Navbar />, document.getElementById('navbar'));
// ReactDOM.render(<Content />, document.getElementById('content'));
// ReactDOM.render(<Game />, document.getElementById('root'));
ReactDOM.render(<Users />, document.getElementById('root'));
registerServiceWorker();