import React, { Component } from 'react';
import Header from './Header';
import TableElement from './TableElement';

class Content extends Component {
  constructor(props) {
    super(props);
    this.state = { isOn: false };
  }

  handleClick(event) {
    if (this.state.isOn) {
      this.setState({isOn : false});
    } else {
      this.setState({isOn : true});
    }
  }

  render() {
    const status = this.state.isOn ? 'On' : 'Off';
    return (
      <div>
        <div>
          <Header />
        </div>
        <div>
          <h3>{status}</h3>
          <button type="button" className="btn btn-primary" onClick={this.handleClick.bind(this)}>
            Change State
          </button>
        </div>
        <div>
          <TableElement />
        </div>
      </div>  
    );
  }
}

export default Content;
