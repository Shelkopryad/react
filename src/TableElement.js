import React, { Component } from 'react';

class TableElement extends Component {

  constructor(props) {
    super(props);
    this.state = { isOn: false };
  }

  render() {
    return (
      <div>
        <table className="table" id="table">
          <thead className="thead-light">
            <tr>
              <th scope="col">#</th>
              <th scope="col">email</th>
              <th scope="col">password</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>qwe123</td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Jacob</td>
              <td>asd345</td>
            </tr>
          </tbody>
        </table>
        <form>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Email</label>
            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
            <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputPassword1">Password</label>
            <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
          </div>
          <button type="submit" className="btn btn-primary" onClick={this.handleClick}>
            Submit
          </button>
        </form>
      </div>
    );
  }
}

export default TableElement;